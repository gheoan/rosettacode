# Rosetta Code

My own solutions to various
[Rosetta Code tasks](https://www.rosettacode.org/wiki/Category:Programming_Tasks).

Solutions are grouped by category, as found
[here](https://www.rosettacode.org/mw/index.php?title=Category:Solutions_by_Programming_Task&oldid=17281).

To find the Page ID of a task,
open the task on Rosseta Code and click on "Page Information" at the bottom of the page.
The ID is then found under "Basic information".

To find a task by Page ID,
use the following URL: `https://www.rosettacode.org/wiki/index.php?curid=<page_id>`.
