use curl::easy::Easy as Session;
use serde::Deserialize;
use serde_json as json;
use std::collections::HashMap;
use std::fs;
use std::path::Path;
use std::path::PathBuf;
use url::Url;

fn symlink(original: impl AsRef<Path>, link: impl AsRef<Path>) -> std::io::Result<()> {
    #[cfg(windows)]
    {
        type BOOLEAN = u8;
        type LPCSTR = *const i8;
        type DWORD = u32;
        extern "system" {
            fn CreateSymbolicLinkA(link: LPCSTR, original: LPCSTR, flags: DWORD) -> BOOLEAN;
            fn GetLastError() -> DWORD;
        }
        use std::ffi::CString;
        let link = CString::new(link.as_ref().as_os_str().to_str().unwrap()).unwrap().into_raw();
        let original = CString::new(original.as_ref().as_os_str().to_str().unwrap()).unwrap().into_raw();
        let result = unsafe { CreateSymbolicLinkA(link, original, 1 | 2) };
        return if result == 0 {
          Err(std::io::Error::from_raw_os_error(unsafe {GetLastError() as _}))  
         } else {
         Ok(())
         }
    }
    #[cfg(unix)]
    {
        return std::os::unix::fs::symlink(original, link);
    }
}

fn title_to_file_name<'a>(title: &str) -> String {
    title
        .chars()
        .map(|c| return if c.is_ascii_alphanumeric() { c } else { '_' })
        .collect()
}

#[derive(Deserialize, Debug)]
struct WikiPage {
    pageid: Option<usize>,
    ns: usize,
    title: String,
    categories: Option<Vec<WikiPage>>,
}

fn transfer_to_json(buffer: &mut Vec<u8>, session: &mut Session) -> json::Value {
    buffer.clear();
    let mut request = session.transfer();
    request
        .write_function(|data| {
            buffer.extend_from_slice(data);
            Ok(data.len())
        })
        .unwrap();
    request.perform().unwrap();
    drop(request);
    json::from_reader(buffer.as_slice()).unwrap()
}

fn main() {
    println!("cargo:rerun-if-changed=solutions_by_id");

    let solutions_by_id_dir = PathBuf::from("..").join("solutions_by_id");
    let mut buffer = Vec::new();
    let mut session = Session::new();
    let base_url = Url::parse("https://www.rosettacode.org/mw/api.php?format=json").unwrap();

    let mut subcategories_url = base_url.clone();
    subcategories_url
        .query_pairs_mut()
        .extend_pairs(&[
            ("action", "query"),
            ("list", "categorymembers"),
            ("cmtype", "subcat"),
            ("cmtitle", "Category:Solutions_by_Programming_Task"),
            ("cmlimit", "500"),
        ])
        .finish();

    session.url(subcategories_url.as_str()).unwrap();
    let mut result = transfer_to_json(&mut buffer, &mut session);

    let subcategories: HashMap<String, PathBuf> =
        json::from_value::<Vec<WikiPage>>(result["query"]["categorymembers"].take())
            .unwrap()
            .iter()
            .map(|subcategory| {
                let title = &subcategory.title;
                (
                    title.clone(),
                    title_to_file_name(&title.replace("Category:", "")).into(),
                )
            })
            .collect();

    let page_ids = fs::read_dir("solutions_by_id")
        .unwrap()
        .filter_map(|entry| entry.ok())
        .map(|entry| entry.path())
        .filter(|path| path.is_dir())
        .filter_map(|path| path.file_stem().map(|s| s.to_owned()))
        .filter_map(|id| id.into_string().ok())
        .collect::<Vec<_>>();

    let mut tasks: HashMap<String, WikiPage> = Default::default();

    for page_id in page_ids {
        let mut page_categories_url = base_url.clone();
        page_categories_url
            .query_pairs_mut()
            .extend_pairs(&[
                ("action", "query"),
                ("prop", "categories"),
                ("pageids", page_id.as_str()),
                ("clshow", "!hidden"),
                ("cllimit", "500"),
            ])
            .finish();

        session.url(page_categories_url.as_str()).unwrap();
        let mut result = transfer_to_json(&mut buffer, &mut session);

        tasks.extend(
            json::from_value::<HashMap<String, WikiPage>>(result["query"]["pages"].take())
                .unwrap()
                .into_iter(),
        );
    }

    for category_directory in subcategories.values() {
        fs::remove_dir_all(&category_directory).ok();
    }

    for category_directory in subcategories
        .iter()
        .filter(|(subcategory, _)| {
            tasks.values().any(|task| {
                task.categories
                    .as_ref()
                    .expect("each task should have a relevant category")
                    .iter()
                    .any(|category| &&category.title == subcategory)
            })
        })
        .map(|(_k, v)| v)
    {
        fs::create_dir(&category_directory).ok();
    }

    let by_title_directory: PathBuf = "solutions_by_title".into();
    fs::remove_dir_all(&by_title_directory).unwrap();
    fs::create_dir(&by_title_directory).unwrap();
    for (task_id, page) in tasks {
        if let Some(page_categories) = page.categories {
            let relevent_categories = page_categories
                .iter()
                .filter(|category| subcategories.contains_key(&category.title));
            if relevent_categories.size_hint().1 == Some(0) {
                println!("cargo:warning=task without relevent categories");
            }
            for category in relevent_categories {
                let category_directory = subcategories.get(&category.title).unwrap();
                symlink(
                    solutions_by_id_dir.join(&task_id),
                    category_directory.join(&title_to_file_name(&page.title)),
                )
                .unwrap();
            }
            symlink(
                solutions_by_id_dir.join(&task_id),
                by_title_directory.join(&title_to_file_name(&page.title)),
            )
            .unwrap();
        } else {
            println!("cargo:warning=task without categories");
        }
    }
}
