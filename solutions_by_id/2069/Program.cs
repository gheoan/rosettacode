﻿using static System.Console;
using static System.Convert;
using static System.Math;

WriteLine("input two integers, one per line");

int i = ToInt32(ReadLine());
int j = ToInt32(ReadLine());

WriteLine(i + j);
WriteLine(i - j);
WriteLine(i * j);
WriteLine("{0} quotient, rounded towards zero", i / j);
if (i * j < 0) {
  WriteLine("{0} remainder, matching the sign of the first integer", i % j);
} else {
  WriteLine(i % j);
}
WriteLine(Pow(i, j));
