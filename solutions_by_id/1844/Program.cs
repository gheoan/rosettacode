﻿using System;

var m = new Person("John");
m.greeting();

class Person {
  string name;

  public Person(string name) {
    this.name = name;
  }

  public void greeting() {
    Console.WriteLine("Hello. My name is {0}!", name);
  }
}
