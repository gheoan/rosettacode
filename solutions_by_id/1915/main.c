#include <stdio.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <threads.h>

// Use a flag so that all threads can start "at the same time".
static atomic_bool flag;

int print(void* str) {
  struct timespec duration = { .tv_sec = 0, .tv_nsec = 1915};
  while(!atomic_load(&flag)) {
    thrd_sleep(&duration, NULL);
  }
  printf("%s\n", str);
  return EXIT_SUCCESS;
}

void expect(int result, char* msg) {
  if (result != thrd_success) {
    printf("%s error %i\n", msg, result);
    exit(EXIT_FAILURE);
  }
}

int main() {
  thrd_t thread[3];
  const char* msg[] = {"Enjoy", "Rosetta", "Code"};
  atomic_init(&flag, false);
  for (unsigned i = 0; i < 3; ++i) {
    expect(thrd_create(&thread[i], print, (void*)msg[i]), "thread create");
  }
  // Sleep to make the scheduler less predictable.
  struct timespec duration = { .tv_sec = 1, .tv_nsec = 915};
  thrd_sleep(&duration, NULL);
  atomic_store(&flag, true);
  for (unsigned i = 0; i < 3; ++i) {
    expect(thrd_join(thread[i], NULL), "thread join");
  }
  return 0;
}