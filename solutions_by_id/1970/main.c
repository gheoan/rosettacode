#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct {
  int* elements;
  size_t count;
  size_t allocated;
} IntStack;

void push_stack(IntStack* stack, int element) {
  if (stack->allocated == stack->count) {
    stack->elements = realloc(stack->elements, (stack->allocated + 10) * sizeof(element));
    stack->allocated += 10;
  }
  stack->elements[stack->count] = element;
  stack->count += 1;
}

int pop_stack(IntStack* stack) {
  auto result = stack->elements[stack->count - 1];
  stack->count -= 1;
  return result;
}

bool empty_stack(IntStack* stack) {
  return stack->count == 0;
}

int main() {
  IntStack stack = {};
  push_stack(&stack, -1);
  push_stack(&stack, 0);
  push_stack(&stack, 1);
  push_stack(&stack, 2);
  push_stack(&stack, 3);
  push_stack(&stack, 4);
  push_stack(&stack, 5);
  push_stack(&stack, 6);
  push_stack(&stack, 7);
  push_stack(&stack, 8);
  push_stack(&stack, 9);
  push_stack(&stack, 10);
  while (!empty_stack(&stack)) {
    printf("%i\n", pop_stack(&stack));
  }
  return 0;
}