cmake_minimum_required(VERSION 3.14)
project(1970 C)

set(CMAKE_C_STANDARD 11)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/out)
add_executable(1970 main.c)
