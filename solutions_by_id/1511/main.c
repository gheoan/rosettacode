#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

int main() {
  FILE* input_fp = fopen("../input.txt", "rb");
  FILE* output_fp = fopen("../output.txt", "wb");
  if (input_fp == NULL || output_fp == NULL) {
    perror("open error");
    return EXIT_FAILURE;
  }

  unsigned char buffer[1024];
  while (feof(input_fp) == 0) {
    size_t read = fread(buffer, 1, 1024, input_fp);
    fwrite(buffer, 1, read, output_fp);
  }
}
