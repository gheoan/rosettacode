#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct {
  float x;
  float y;
} Point;

typedef struct {
  Point p;
  unsigned result;
  Point vertices[];
} Test;

int main(int argc, char* argv[]) {
  Point p = { atof(argv[1]), atof(argv[2]) };
  unsigned vertex_count = (argc - 4) / 2;
  Point* vertices = malloc(vertex_count * sizeof(Point));
  for(unsigned i = 0; i < vertex_count; i += 1) {
    Point v = { atof(argv[3 + i * 2]), atof(argv[3 + i * 2 + 1]) };
    vertices[i] = v;
  }
  unsigned count = 0;
  for (unsigned i = 0 ; i < vertex_count; i += 1) {
    unsigned j = i + 1 == vertex_count ? 0 : i + 1;
    if ((vertices[i].x > p.x || vertices[j].x > p.x) &&
      (vertices[i].y - p.y) * (vertices[j].y - p.y) <= 0) {
      count += 1;
    }
  }
  if(count % 2 == atoi(argv[argc - 1])) {
    printf("passed\n");
  } else {
    printf("failed\n");
  }
}
