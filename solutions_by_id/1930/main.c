#include <stdio.h>
#include <threads.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdatomic.h>

// TODO: do not spinlock

enum PrintMessageType {
  PrintMessageTypePrintRequest,
  PrintMessageTypeCountRequest,
};

struct PrintMessage {
  enum PrintMessageType type;
  char *value;
};

struct PrintQueue {
  struct PrintMessage *array;
  unsigned array_length;
};

struct PrintCountResponse {
  bool reading_done;
  unsigned printed_count;
};

struct ThreadCommunication {
  struct PrintQueue *queue;
  struct PrintCountResponse *done;
};

void print_queue_push(struct PrintQueue *queue, struct PrintMessage message) {
  // TODO: length check
  queue->array[queue->array_length] = message;
  atomic_thread_fence(memory_order_release);
  queue->array_length += 1;
}

int read_thrd_func(struct ThreadCommunication *communication) {
  struct PrintQueue *print_queue = communication->queue;
  struct PrintCountResponse *reading_done = communication->done;
  FILE *fp = fopen("../input.txt", "r");
  if (!fp) {
    return EXIT_FAILURE;
  }
  // Does not support lines longer then 256 characters.
  // TODO: find a way to detect when fgets did not stop on a new line.
  while (true) {
    char *string = malloc(256);
    if (fgets(string, 256, fp)) {
      struct PrintMessage message = {
          .type = PrintMessageTypePrintRequest,
          .value = string,
      };
      print_queue_push(print_queue, message);
    } else {
      struct PrintMessage message = {.type = PrintMessageTypeCountRequest};
      print_queue_push(print_queue, message);
      break;
    }
  }
  while (true) {
    if (reading_done->reading_done) {
      printf("\nPrinted %u lines.\n", reading_done->printed_count);
      break;
    }
  }
  return EXIT_SUCCESS;
}

int print_thrd_func(struct ThreadCommunication *communication) {
  struct PrintQueue *print_queue = communication->queue;
  struct PrintCountResponse *reading_done = communication->done;
  unsigned processed_count = 0;
  while (true) {
    if (processed_count < print_queue->array_length) {
      struct PrintMessage message = print_queue->array[processed_count];
      switch (message.type) {
      case PrintMessageTypePrintRequest: {
        printf("%s", message.value);
        break;
      }
      case PrintMessageTypeCountRequest: {
        reading_done->printed_count = processed_count;
        atomic_thread_fence(memory_order_release);
        reading_done->reading_done = true;
        return EXIT_SUCCESS;
      }
      default: { break; }
      }
      processed_count += 1;
    }
  }
}

int main() {
  setbuf(stdout, NULL);
  thrd_t read_thrd;
  // Maximum supported number of lines in the input is around 256.
  struct PrintQueue queue = {
      .array = malloc(256 * sizeof(struct PrintMessage)),
      .array_length = 0,
  };
  struct PrintCountResponse done = {
      .reading_done = false,
  };
  struct ThreadCommunication communication = {
      .queue = &queue,
      .done = &done,
  };
  thrd_t print_thrd;
  thrd_create(&read_thrd, (thrd_start_t) read_thrd_func, &communication);
  thrd_create(&print_thrd, (thrd_start_t) print_thrd_func, &communication);
  // TODO: handle return value from thread function
  thrd_join(read_thrd, NULL);
  thrd_join(print_thrd, NULL);
  return EXIT_SUCCESS;
}