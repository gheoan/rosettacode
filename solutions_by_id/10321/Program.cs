﻿using System;

var m = new Person(333);
m.greeting();

var n = Person.cloneFrom(m);
n.greeting();

class Person {
  uint name;

  public Person(uint name) {
    this.name = name;
  }

  public static Person cloneFrom(Person p) {
    return new Person(p.name + 1);
  }

  public void greeting() {
    Console.WriteLine("Hello. My name is {0}!", name);
  }
}
