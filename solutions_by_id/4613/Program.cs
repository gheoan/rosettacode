﻿using static System.Diagnostics.Debug;
using System;
using System.Collections.Generic;

// fixed length array
var a = new byte[20];
// initialized to 0
Assert(a[4] == 0);
Assert(a.Length == 20);
a[2] = 30;
Console.WriteLine(a[2]);
var b = new string[2];
Assert(b[1] == null);

// dynamic length array
var l = new List<nuint>(50);
Assert(l.Count == 0);
Assert(l.Capacity == 50);
l.Add(111);
l.Add(333);
l[1] = 222;
Assert(l[0] == 111);
Assert(l[1] == 222);
