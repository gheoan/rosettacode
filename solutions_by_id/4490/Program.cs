﻿using static System.Diagnostics.Debug;

bool a = true;
var b = false;
Assert(a != b);
Assert(true == true);
Assert(true != false);
// Assert(0 != false); // compiler error
