﻿#nullable enable

using System;
using static System.Diagnostics.Debug;

Assert(0 != null);

string s1 = "";
Assert(s1 != null);

string s2 = null;
Assert(s2 == null);

Assert(s1 != s2);

int? i = null + 1;
Assert(i == null);
Assert((i ?? 0) == 0);
i = 1;
Assert(i.GetType() == typeof(int));

Assert((ushort?)null == null);
Assert((string?)null == null);

Assert(!(null is object));
