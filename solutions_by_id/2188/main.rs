use std::mem;
use mem::MaybeUninit;

struct Queue<T> {
  // NOTE: length must not be 0, else push panics
  array: [MaybeUninit<T>; 2],
  front: usize,
  length: usize,
}

impl<T> Queue<T> {
  // TODO: inline functions
  // TODO: overflowing front or length
  // TODO: unsafe usage
  // TODO: do not bound check on array access
  pub fn new() -> Self {
    return Self {
      array: unsafe { MaybeUninit::uninit().assume_init() },
      front: 0,
      length: 0,
    };
  }
  pub fn empty(&self) -> bool {
    return self.length == 0;
  }
  pub fn push(&mut self, element: T) {
    let i = (self.front + self.length) % self.array.len();
    self.array[i] = MaybeUninit::new(element);
    self.length += 1;
  }
  pub fn pop(&mut self) -> Option<T> {
    return (!self.empty()).then(|| {
      let new_v = unsafe { MaybeUninit::uninit().assume_init() };
      let i = self.front % self.array.len();
      // TODO: a better way to get an owned value out of the array
      let old_v = std::mem::replace(&mut self.array[i], new_v);
      self.front += 1;
      self.length -= 1;
      return unsafe { mem::transmute_copy(&old_v) };
    });
  }
}

fn main() {
  let mut q = Queue::new();
  assert_eq!(q.pop(), None);
  q.push(54);
  q.push(32);
  assert_eq!(q.pop(), Some(54));
  q.push(10);
  assert_eq!(q.pop(), Some(32));
  assert_eq!(q.pop(), Some(10));
  assert_eq!(q.pop(), None);
}
