#include <stdio.h>
#include <stddef.h>

void quicksort(int* a, size_t end) {
  if(end > 1) {
    auto pivot = end - 1;
    size_t i = 0;
    while(i < pivot) {    
      if(a[i] > a[pivot]) {
        auto tmp = a[pivot];
        a[pivot] = a[i];
        a[i] = tmp;
        pivot -= 1;
   
        if(pivot != i) {
          auto tmp = a[pivot];
          a[pivot] = a[i];
          a[i] = tmp;
        }
      } else {
        i += 1;
      }
    }
    quicksort(a, pivot);
    quicksort(a + pivot + 1, end - 1 - pivot);
  }
}

int main() {
  #define COUNT 20
  int a[COUNT] = {};
  for(size_t i = 0; i < COUNT; ++i) {
    auto c = i % 2 ? -3 : 7;
    a[i] = (COUNT - i) / 2 + c;
    printf("%i ", a[i]);
  }
  printf("\n");
  
  quicksort(a, COUNT);
  
  for(size_t i = 0; i < COUNT; ++i) {
    printf("%i ", a[i]);
  }
  printf("\n");
}