﻿using static System.Console;

var a = Animal.Dog;

if (a == Animal.Cat) {
  WriteLine("cat");
} else if (a == Animal.Dog) {
  WriteLine("dog");
} else {
  WriteLine("unreachable!");
}

switch (a) {
  case Animal.Cat: {
    WriteLine("cat");
    break;
  }
  case Animal.Dog: WriteLine("dog"); break;
  default: {
    WriteLine("unreachable");
    break; // required in "default" case
  }
}

enum Animal {
  Cat,
  Dog,
}
